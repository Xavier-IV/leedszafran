package leeds.lab2.studentgrades;

public class StudentGrades {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		StudentGrades wk2 = new StudentGrades();
		wk2.printBorder();
		wk2.header();
		
		wk2.printName("Joe",43,7);
		wk2.printName("William",50,8);
		wk2.printName("Mary Sue",39,10);
		wk2.printName("Samual Brodvski",49,9);
		wk2.printName("Sam",29,3);
		wk2.printName("James",39, 6);
	}
	
		
	public void header(){
		System.out.println("\nName\t\t  Lab\tBonus\tTotal");
		System.out.println("----\t\t  ---\t-----\t-----");
	}
	
	public void printName(String name, int lab, int bonus){
		String tab = "\t\t";
		if(name.length()>7){
			tab = "\t";
		}
		System.out.println(name + tab + "  "+ lab +"\t"+ bonus +"\t"+(lab+bonus));
	}
	
	public void printBorder(){
		for(int x = 0; x < 20; x++){
			System.out.print("/");
		}
		for(int y = 0; y < 20; y++){
			System.out.print("\\");
		}
		System.out.println("\n== \t     Student Points \t      ==");
		for(int x = 0; x < 20; x++){
			System.out.print("\\");
		}
		for(int y = 0; y < 20; y++){
			System.out.print("/");
		}
	}

}
