package leeds.lab2.squarecalc;

import java.util.Scanner;

public class SquareCalc {

	int perimeter = 0, area = 0, newLength;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SquareCalc sc = new SquareCalc();
		
		sc.newLength = sc.promptLength();
		
		sc.area = sc.calcArea(sc.newLength);
		sc.perimeter = sc.calcPerimeter(sc.newLength);
		
		System.out.println("Perimeter is "+sc.perimeter+" cm\nArea is "+sc.area+" cm^2.");
		
	}
	
	public int promptLength(){
		System.out.print("Please enter the length of the square(cm):");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		int length = input.nextInt();
		
		return length;
	}
	
	public int calcPerimeter(int length){
		int perimeter = length * 4;
		
		return perimeter;
	}
	
	public int calcArea(int length){
		int area = (int) (Math.pow(length, 2));
		
		return area;
	}

}
