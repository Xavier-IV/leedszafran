package leeds.lab2.compaverages;

import java.util.Scanner;

public class CompAverages {

	public static void main(String[] args)
	{	
       int val1, val2, val3;
       double average;
       @SuppressWarnings("resource")
	Scanner scan = new Scanner(System.in) ;

       // get three values from user
       System.out.println("Please enter three integers and " +
                       "I will compute their average");
       System.out.println("First value");
       val1 = scan.nextInt();
       
       System.out.println("Second value");
       val2 = scan.nextInt();
       
       System.out.println("Third value");
       val3 = scan.nextInt();
	  
       //compute the average

      average = (val1 + val2 + val3)/3;

       //print the average

       System.out.println("The average is "+average);

    }


}
