package leeds.lab2.fraction;

import java.util.Scanner;

public class Fraction {

	float num, denum, newNum, newDenum, deci;
	
	public static void main(String[] args) {
		Fraction fc = new Fraction();
		
		fc.newNum = fc.getNum();
		fc.newDenum = fc.getDenum();

		System.out.print("\nThe decimal for the fraction is ");
		fc.deci = fc.printDecimal(fc.newNum, fc.newDenum);
		System.out.println(fc.deci);
		
	}
	
	public float getNum(){
		
		System.out.print("Please enter numerator   :");
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);

		num = input.nextInt();
		return num;
	}

	public float getDenum(){
		
		System.out.print("Please enter de-numerator:");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		denum = input.nextInt();
		return denum;
	}
	
	public float printDecimal(float newNum2, float newDenum2){
		
		deci = newNum2/newDenum2;
		
		return deci;
		
	}
}
