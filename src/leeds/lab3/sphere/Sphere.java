package leeds.lab3.sphere;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Sphere {

	final double PI = Math.PI;
	double r,volume,area;
	
	
	Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Sphere sp = new Sphere();
		System.out.println(sp.PI);
		
		sp.getRad();
		sp.calcVolume(sp.r, sp.PI);
		sp.calcArea(sp.r, sp.PI);
		DecimalFormat f = new DecimalFormat("#.0000");

		System.out.println("Area is "+f.format(sp.area));
		System.out.println("Volume is "+f.format(sp.volume));
	}	
	
	public double getRad(){
		System.out.println("Please enter your radius");
		
		r = input.nextDouble();
		
		return r;
	}
	
	public double calcVolume(double r, double PI){
		volume = 4 * PI * (Math.pow(r, 3)) / 3;
		
		return volume;
	}
	
	public double calcArea(double r, double PI){
		area = 4 * PI * (Math.pow(r, 2));
		
		return volume;
	}
	

}
