package leeds.practice.square;

import java.util.Scanner;
import java.math.*;

public class square {

	public static void main(String[] args) {
		int a,b,c;
		
		double discrim, root1, root2;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the coeefficient of x squared:");
		a = scan.nextInt();
		
		System.out.println("Enter the coefficient of x: ");
		b = scan.nextInt();
		
		
		System.out.print("Enter the constant : ");
		c = scan.nextInt();
		
		discrim = Math.pow(b,  2) - (4 * a * c);
		
		root1 = ((-1 * b) + Math.sqrt(discrim)) / (2 * a);
		root2 = ((-1 * b) - Math.sqrt(discrim)) / (2 * a);
				
		System.out.println("Root #1: " + root1);
		System.out.println("Root #2: " + root2);
	}

}
